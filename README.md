# Kubernetesize Fontane

This repository contains all information to serve the Fontane-Notizbuecher application from kubernetes.

## Docker

The dockerfile is based on a generic exist-db docker image provided by SUB Göttingen. For details on the
base image, its flavours and registry look at [sepia/existdb-images](https://gitlab.gwdg.de/sepia/existdb-images).

We are using a debug image currently that provides a shell in addition to the java runtime environment.

### Usage

```sh
docker build --tag harbor.gwdg.de/sub-fe-pub/fontane:develop . &&
docker push harbor.gwdg.de/sub-fe-pub/fontane:develop
```

## Helm Chart

The directory „fontane“ contains the helm chart.

### Usage

```sh
helm package fontane &&
curl https://harbor.gwdg.de/api/chartrepo/sub-fe-pub/charts -F "chart=@$(ls -Art fontane-*.tgz | tail -n 1)" -u 'robot:token'
```

## Deployment

Argo CD is pulling the recent helm chart version automatically. The Argo app deployment is managed at
[subugoe/argocd-provisionin](https://gitlab.gwdg.de/subugoe/argocd-provisioning).
