# https://harbor.gwdg.de/harbor/projects/7/repositories/existdb/artifacts-tab
FROM harbor.gwdg.de/sub-fe/existdb@sha256:78374848b33d13e9e92667d6d6c8502b108e3ce258ed036716236ea5442963ee AS base

# 6.0.1 - debug
FROM harbor.gwdg.de/sub-fe/existdb@sha256:3dab74ebab2f075dc2d13072fa2e12762b06c54814941760467f420637a748d0 AS develop

ADD https://gitlab.gwdg.de/fontane-notizbuecher/static/-/archive/master/static-master.tar /exist/etc/jetty/webapps/portal/
RUN tar -xf /exist/etc/jetty/webapps/portal/static-master.tar --strip-components=1 --directory /exist/etc/jetty/webapps/portal

ENV PROCESSOR="6.0.1"

# for exist 6.0.1
ADD http://exist-db.org/exist/apps/public-repo/public/shared-resources-0.9.1.xar /exist/autodeploy/shared-resources.xar
ADD http://exist-db.org/exist/apps/public-repo/public/markdown-1.0.0.xar /exist/autodeploy/markdown.xar

ADD https://ci.de.dariah.eu/exist-repo/find?abbrev=cv-develop&processor=${PROCESSOR} /exist/autodeploy/cv.xar
ADD https://ci.de.dariah.eu/exist-repo/find?abbrev=openapi-develop&processor=${PROCESSOR} /exist/autodeploy/openapi.xar
# Fontane-dev: 5.7.3
ADD https://ci.de.dariah.eu/exist-repo/find?abbrev=fontane-develop&processor=${PROCESSOR} /exist/autodeploy/fontane.xar
# TG-Connect-dev: 5.2.2
ADD https://ci.de.dariah.eu/exist-repo/find?abbrev=tgconnect-develop&processor=${PROCESSOR} /exist/autodeploy/tgconnect.xar
# Fontane-dev: 5.4.3
ADD https://ci.de.dariah.eu/exist-repo/find?abbrev=SADE-fontane-develop&processor=${PROCESSOR} /exist/autodeploy/SADE-fontane.xar

# 6.0.1 - production
FROM base AS main
ENV PROCESSOR="6.0.1"

COPY --from=develop /exist/etc/jetty/webapps/portal /exist/etc/jetty/webapps/portal
COPY --from=develop /exist/autodeploy/shared-resources.xar /exist/autodeploy/shared-resources.xar
COPY --from=develop /exist/autodeploy/markdown.xar /exist/autodeploy/markdown.xar
COPY --from=develop /exist/autodeploy/cv.xar /exist/autodeploy/cv.xar
COPY --from=develop /exist/autodeploy/openapi.xar /exist/autodeploy/openapi.xar

# TG-Connect: 5.2.2
ADD https://ci.de.dariah.eu/exist-repo/find?abbrev=tgconnect&processor=${PROCESSOR} /exist/autodeploy/tgconnect.xar
# Fontane: 5.7.3
ADD https://ci.de.dariah.eu/exist-repo/find?abbrev=fontane&processor=${PROCESSOR} /exist/autodeploy/fontane.xar 
# SADE-Fontane: 5.4.3
ADD https://ci.de.dariah.eu/exist-repo/find?abbrev=SADE-fontane&processor=${PROCESSOR} /exist/autodeploy/SADE-fontane.xar
